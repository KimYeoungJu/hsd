#include "fpga_api.h"
#include <cstring>
#include <cstdio>
#include <cstdlib>

#define DATA_SIZE SIZE*(SIZE+1) // fpga bram data size

#define min(x,y) (((x)<(y))?(x):(y))

FPGA::FPGA(off_t data_addr, off_t api_addr)
{
    api_ = new unsigned int[SIZE];    // use api_ as tempolar output 
    data_ = new float[DATA_SIZE];	
}

FPGA::~FPGA()
{
    delete[] api_;
    delete[] data_;
}

float* FPGA::matrix(void)
{
	return data_ + SIZE;
}

float* FPGA::vector(void)
{
	return data_;
}

const float* FPGA::run()
{
	float* vec = this->vector();
	float* mat = this->matrix();
	float* out  = reinterpret_cast<float*>(api_);  

	for(int i = 0 ; i < SIZE; ++i)
	{
		out[i] = 0;

		for(int j = 0 ; j < SIZE; ++j)
			out[i] += vec[j] * mat[SIZE*i + j];
	}

	for(int i = 0 ; i < SIZE; ++i)
	{
		data_[i] = out[i];
	}

	return data_;    
}

void FPGA::largeMV(const float* large_mat, const float* input,
		float* output, int M, int N)
{
	float* vec = this->vector();
	float* mat = this->matrix();
	int X = M % SIZE == 0 ? (M / SIZE) * SIZE : (M / SIZE + 1) * SIZE;
	int Y = N % SIZE == 0 ? (N / SIZE) * SIZE : (N / SIZE + 1) * SIZE;
	int i, j, k;
	float* zpmat = new float[X * Y];
	float* zpvec = new float[X];
	float* zpout = new float[Y];

	for(i = 0; i < N; i += 1) {
		for(j = 0; j < M; j += 1) {
			zpmat[i * X + j] = large_mat[i * M + j];
			//printf("%f\n", zpmat[i * X + j])i;
		}
	}
	
	for(i = 0; i < M; i += 1) {
		zpvec[i] = input[i];
	}

	for(j = 0; j < X; j += SIZE) {

		//printf("-----------------------------");

		for(i = 0; i < Y; i += SIZE) {
			for(k = 0; k < SIZE; k += 1) {
				*(vec + k) = zpvec[j + k];
				//printf("%f\n", calcvec[k]);
			}

			for(k = 0; k < SIZE * SIZE; k += 1) {
				*(mat + k) = zpmat[(i + k / SIZE) * X + j + k % SIZE];
				//printf("%f\n", calcmat[k]);
			}

			for(k = 0; k < DATA_SIZE; k += 1) {
				printf("%f\n", *(this->data_ + k));
			}

			this->run();

			for(k = 0; k < SIZE; k += 1) {
				*(zpout + i + k) += *(this->data_ + k);
			}
		}
	}
	
	for(i = 0; i < N; i += 1) {
		*(output + i) = *(zpout + i);
	}
}

