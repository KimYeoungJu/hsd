#include "fpga_api.h"
#include <cstring>

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#define DATA_SIZE SIZE*(SIZE+1)*sizeof(float) // fpga bram data size

#define min(x,y) (((x)<(y))?(x):(y))

FPGA::FPGA(off_t data_addr, off_t api_addr)
{
    fd_ = open("/dev/mem", O_RDWR);
    data_ = static_cast<float*>(mmap(NULL, DATA_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd_, data_addr));
    api_ = static_cast<unsigned int*>(mmap(NULL, sizeof(unsigned int), PROT_READ|PROT_WRITE, MAP_SHARED,fd_, api_addr));
}

FPGA::~FPGA()
{
    munmap(data_, DATA_SIZE );
    munmap(api_, sizeof(unsigned int));
    close(fd_);
}

float* FPGA::matrix(void)
{
	return data_ + SIZE;
}

float* FPGA::vector(void)
{
	return data_;
}

const float* __attribute__((optimize("O0"))) FPGA::run()
{
    *api_ = 0x5555;
    while(*api_ == 0x5555);

    return data_;    
}

void FPGA::largeMV(const float* large_mat, const float* input,
		float* output, int M, int N)
{
	float* vec = this->vector();
	float* mat = this->matrix();
	int X = M % SIZE == 0 ? (M / SIZE) * SIZE : (M / SIZE + 1) * SIZE;
	int Y = N % SIZE == 0 ? (N / SIZE) * SIZE : (N / SIZE + 1) * SIZE;
	int i, j, k;
	float* zpmat = new float[X * Y];
	float* zpvec = new float[X];
	float* zpout = new float[Y];

	for(i = 0; i < N; i += 1) {
		for(j = 0; j < M; j += 1) {
			zpmat[i * X + j] = large_mat[i * M + j];
			//printf("%f\n", zpmat[i * X + j])i;
		}
	}
	
	for(i = 0; i < M; i += 1) {
		zpvec[i] = input[i];
	}

	for(j = 0; j < X; j += SIZE) {

		//printf("-----------------------------");

		for(i = 0; i < Y; i += SIZE) {
			for(k = 0; k < SIZE; k += 1) {
				*(vec + k) = zpvec[j + k];
				//printf("%f\n", calcvec[k]);
			}

			for(k = 0; k < SIZE * SIZE; k += 1) {
				*(mat + k) = zpmat[(i + k / SIZE) * X + j + k % SIZE];
				//printf("%f\n", calcmat[k]);
			}

			for(k = 0; k < DATA_SIZE; k += 1) {
				printf("%f\n", *(this->data_ + k));
			}

			this->run();

			for(k = 0; k < SIZE; k += 1) {
				*(zpout + i + k) += *(this->data_ + k);
			}
		}
	}
	
	for(i = 0; i < N; i += 1) {
		*(output + i) = *(zpout + i);
	}		
}
